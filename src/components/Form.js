import React from 'react'
import PropTypes from 'prop-types'

class Form extends React.Component {
  render() {
    return (
      <>
        <form onSubmit={this.props.onSubmit} className="form">
          <label>
            New to-do list
            <input
              className="form__input"
              onChange={this.props.onChange}
              type="text"
              value={this.props.value}
              placeholder="Add to-do here"
            />
            <button className="form__button">Add to-do</button>
          </label>
        </form>
      </>
    )
  }
}

Form.defaultProps = {
  onSubmit: () => {},
  onChange: () => {},
  value: ''
}

Form.propTypes = {
  onSubmit: PropTypes.func,
  onChange: PropTypes.func,
  value: PropTypes.string
}

export default Form
