import React from 'react'
import PropTypes from 'prop-types'

const CheckBox = props => {
  return (
    <>
      <input type="checkbox" checked={props.checked} ref={props.onChange} />
    </>
  )
}

CheckBox.propTypes = {
  checked: PropTypes.bool,
  onChange: PropTypes.func
}

CheckBox.defaultProps = {
  checked: false
}

export default CheckBox
