import React from 'react'
import PropTypes from 'prop-types'

class ToDoList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      editMode: false
    }
    this.inputRef = React.createRef()
  }
  handleOnCheck = event => {
    this.props.onCheckChange(this.props.id, event.target.checked)
  }

  onDelete = () => {
    this.props.onDelete(this.props.id)
  }

  onEdit = () => {
    this.setState({
      editMode: !this.state.editMode
    }),
      () => {
        if (this.state.editMode) {
          this.inputRef.current.focus()
        }
      }
  }

  onKeyUp = event => {
    if (event.key === 'Enter') {
      this.setState({
        editMode: false
      })
    }
  }

  onBlur = event => {
    if (event.key === 'Enter') {
      this.setState({
        editMode: false
      })
    }
  }

  onUpdate = event => {
    this.props.onUpdate(event.target.value, this.props.id)
  }

  onEditTo = event => {
    this.props.onEdit(event.target.value)
  }

  render() {
    return (
      <div className="item__">
        <li className="item__list">
          <input
            className="item__checkbox"
            type="checkbox"
            checked={this.props.checked}
            onChange={this.handleOnCheck}
          />
          <div className="item__description">
            {this.state.editMode ? (
              <input
                className="form__input"
                type="text"
                value={this.props.term}
                onChange={this.onUpdate}
                ref={this.inputRef}
                onKeyDown={this.onKeyUp}
                onBlur={this.onBlur}
              />
            ) : (
              <>
                {this.props.checked ? (
                  <del>{this.props.term}</del>
                ) : (
                  <>
                    <span>{this.props.term}</span>
                  </>
                )}
              </>
            )}
          </div>
          <button className="edit__button" onClick={this.onEdit}>
            Edit
          </button>
          <button className="delete__button" onClick={this.onDelete}>
            Delete
          </button>
        </li>
      </div>
    )
  }
}

ToDoList.propTypes = {
  term: PropTypes.string,
  checked: PropTypes.bool,
  onCheckChange: PropTypes.func,
  id: PropTypes.number,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
  onUpdate: PropTypes.func
}

ToDoList.defaultProp = {
  term: 'Add a reminder',
  checked: false
}

export default ToDoList
