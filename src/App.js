import React from 'react'
import { hot } from 'react-hot-loader'
import ToDoList from './components/ToDoList'
import Form from './components/Form'

class App extends React.Component {
  state = {
    term: '',
    //checked: false,
    items: [],
    selectedValue: 'all',
    id: '' // use this as default
  }

  onSubmit = event => {
    event.preventDefault()
    const newItem = {
      term: this.state.term,
      checked: false,
      id: this.state.items.length
    }
    this.setState({
      term: '',
      items: [...this.state.items, newItem]
    })
  }

  onChange = event => {
    this.setState({ term: event.target.value })
  }

  onCheckChange = (id, isChecked) => {
    const tempItems = this.state.items.map(item => {
      if (item.id === id) {
        return { ...item, checked: isChecked }
      }
      return item
    })

    this.setState({ items: [...tempItems] })
  }

  onSelectChange = event => {
    this.setState({
      selectedValue: event.target.value
    })
  }

  onDelete = id => {
    this.setState({
      items: this.state.items.filter(item => item.id !== id)
    })
  }

  onUpdate = (term, id) => {
    const newOb = this.state.items.map(item => {
      if (item.id === id) {
        return { ...item, term: term }
      }
      return item
    })
    this.setState({ items: [...newOb] })
  }

  onEdit = (id, newTerm) => {
    this.setState({
      term: newTerm,
      id: id
    })
  }

  render() {
    let items = []
    switch (this.state.selectedValue) {
      case 'open':
        items = this.state.items.filter(item => !item.checked)
        break
      case 'completed':
        items = this.state.items.filter(item => item.checked)
        break
      default:
        items = this.state.items
        break
    }

    const todoList = items.map(item => {
      return (
        <ToDoList
          term={item.term}
          key={item.id}
          checked={item.checked}
          onCheckChange={this.onCheckChange}
          id={item.id}
          onDelete={this.onDelete}
          onUpdate={this.onUpdate}
          onEdit={this.onEdit}
        />
      )
    })
    return (
      <div className="container">
        <h1 className="grid-1 title">TO Do List Application</h1>
        <Form
          onSubmit={this.onSubmit}
          onChange={this.onChange}
          value={this.state.term}
        />
        <div>
          <select onBlur={this.onSelectChange}>
            <option value="all">All</option>
            <option value="completed">Completed</option>
            <option value="open">Open</option>
          </select>
        </div>
        <ul>{todoList}</ul>
      </div>
    )
  }
}

export default hot(module)(App)
